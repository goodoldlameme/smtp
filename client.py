# -*- coding: utf-8 -*-
import socket
import ssl
import base64
import mimetypes
import json
import re
import argparse


def loadfromconf(filename):
    with open(filename, 'r', encoding="utf-8") as file:
        return json.load(file)


def send_recv(str, s):
    str += b'\n'
    try:
        s.send(str)
        rsv = s.recv(1024)
    except Exception as e:
        return f'Some exception occured!\nType:{type(e).__name__}\nArgs:{e.args}'
    return rsv.decode(encoding='utf-8')


def create_content(filename):
    content_type = mimetypes.MimeTypes().guess_type(filename)[0]
    with open(filename, 'rb') as file:
        att = base64.b64encode(file.read())

    return f'--bound.195987.web7g\r\n' \
           f'Content-Disposition: attachment; filename="{filename}"\r\n' \
           f'Content-Transfer-Encoding: base64\r\n' \
           f'Content-Type: {content_type}; name="{filename}"\r\n' \
           f'\r\n'.encode(encoding='utf-8') + att + '\r\n'.encode(encoding='utf-8')


def create_msg(login, to, subject, text_filename, *attachments_filenames):
    with open(text_filename, 'r', encoding="utf-8") as file:
        text = ''
        lines = file.readlines()
        for line in lines:
            text += '.' + line if re.match('^\.+.*[\n]*$', line) else line
    print(text)

    result = f'From: {login}\r\n' \
             f'To: {to}\r\n' \
             f'Subject: {subject}\r\n' \
             f'Content-Type: multipart/mixed; boundary=bound.195987.web7g\r\n' \
             f'\r\n' \
             f'--bound.195987.web7g\r\n' \
             f'Content-Type: text/plain; charset=utf-8\r\n' \
             f'\r\n' \
             f'{text}\r\n' \
             f'\r\n'.encode(encoding='utf-8')
    for filename in attachments_filenames:
        result += create_content(filename)
    return result + '--bound.195987.web7g--\r\n.'.encode(encoding='utf-8')

def sayhi(login, s):
    print(send_recv(b'EHLO ' + login.encode(), s))

def auth(login, password, s):
    print(send_recv(b'AUTH LOGIN', s))
    print(send_recv(base64.b64encode(login.encode()), s))
    print(send_recv(base64.b64encode(password.encode()), s))

def sendmail(login, conf, s):
    print(send_recv(b'MAIL FROM: ' + login.encode(), s))
    for rcpt in conf['To']:
        print(send_recv(f'RCPT TO: {rcpt}'.encode(), s))
        print(send_recv(b'DATA', s))
        print(send_recv(create_msg(login, rcpt, conf['Subject'], 'stuff\message.txt', *conf['Attachments']), s))

def saygoodbye(s):
    print(send_recv(b'QUIT', s))

def run_smtp(login, password, server: tuple=('smtp.yandex.ru', 465)):
    conf = loadfromconf('stuff\mail.conf')
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s = ssl.wrap_socket(s)
        s.connect(server)
        s.settimeout(10)
        sayhi(login, s)
        auth(login, password, s)
        sendmail(login, conf, s)
        saygoodbye(s)

def main():
    parser = argparse.ArgumentParser(description='Simple AS tracer')
    parser.add_argument('login', type=str, help='email login')
    parser.add_argument('password', type=str, help='email password')
    parser.add_argument('-server', type=str, help='smtp server name', default='smtp.yandex.ru')
    parser.add_argument('-port', type=int, help="smtp server's port", default=465)
    args = parser.parse_args()
    run_smtp(args.login, args.password, (args.server, args.port))


if __name__ == '__main__':
    main()